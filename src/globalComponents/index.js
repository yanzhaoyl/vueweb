// 注册全局组件
import Vue from "vue"
import BackTop from "../components/backTop/BackTop"
import AccountLogin from "../components/accountLogin/AccountLogin"
import PhoneLogin from "../components/phoneLogin/PhoneLogin"


// 回到顶部组件 [组件的名字  组件的实例]
Vue.component('backTop',BackTop)

// 登录组件[普通登录] [组件的名字  组件的实例]
Vue.component('accountLogin',AccountLogin)

// 登录组件[手机验证码登录] [组件的名字  组件的实例]
Vue.component('phoneLogin',PhoneLogin)